# Impressive Founder Detector
## Motivation

Every day, many new startups are founded, but few have the leadership to achieve venture backed returns. What if there were a way that one could find out not only which new startups were founded each day, but also filter down to just those with impressive founders? This gives the basic outline for a tool that could be used by a venture capital firm in order to identify newly founded companies with impressive founders.

## Pipeline
We obtain data on companies and detect impressive founders by by the following pipeline

1) We first must identify all the "new" companies. For the purpose of this exercise, new companies are ones that have been newly added to Mattermark. (Other data sources could easily be used with slight modifications.) We pull info about these companies from Mattermark's API, including the company's webpage location.

2) In order to gather info on the company's team, we scour its webpage (looking at several relevant paths like `/about`, `/company`, and `/team`) for Linkedin profiles.

3) We then crawl Linkedin to get info about the members of this team.

4) These backgrounds are passed through filters that checks for two things: first, that the person is in fact a "founder", and second that they have an "impressive" background, as determined by education and work history. Companies that have individuals meeting this criteria are flagged for the user.

## Sample Filter Criteria

The traits that we consider impressive for a founder are encoded in the following variables in `main.py`:

```
roles = ["professor", "managing partner"]
schools = ["harvard", "stanford", "yale", "berkeley", "mit", "massachussets institute of technology", "wharton"]
degrees = ["phd", "doctorate"]
companies = ["google", "mckinsey", "facebook", "goldman", "addepar"]
```

If a founder's background matches any of these (i.e. they attend Yale University or had job title Associate Professor), then this program considers them "impressive".


## Sample output

Running this on the date "2017-11-17" eventually produces the following results:

```
ZILLIQA has impressive founder Xinshu Dong (https://www.linkedin.com/in/xinshudong/) who ['obtained degree Doctor of Philosophy (PhD)']
ZILLIQA has impressive founder Max Kantelia (https://www.linkedin.com/in/maxkantelia/) who ['had title of Managing Partner']
Ncardia has impressive founder Stefan Braam (https://www.linkedin.com/in/stefanrbraam) who ['obtained degree PhD']
```

Examining the Linkedin profiles of these individuals, we see that these are in fact impressive founders!

The intermediate data produced by several intermediate steps for this date can be seen in `/data` directory of this repository.

In its current state, this program surely missed out on some potentially impressive founders for reasons discussed in the "Potential Improvements" section below.


## How to use
In order to use this program, one must edit `main.py` to include the following

1. A valid Mattermark API key. (I used a free trial for developing.)

2. Linkedin credentials

3. The location of directories where you would like intermediate data to be written.

4. The date on which you would like to run the program, in the format `YYYY-MM-DD`

TODO: add a requirements.txt to install all the pip modules used here.

With the config in place, and the pip modules installed, one can simply run `python main.py`.

## Potential Improvements
There are many ways that this program could be improved. Most notably:

* Many company sites will not have their founders Linkedin profiles listed on them, and thus their founders will not be evaluated. Linkedin profiles should instead be found using Google search or directly from the Linkedin API. This would hugely increase the number of matches found.

* The Linkedin data is parsed with extremely simple regex matching, and detected items are interpreted mostly without context, i.e. the school attended and the degree earned are processed separately when they are clearly related ideas.

* Many other data sources including Crunchbase and Pitchbook should be integrated at various stages of this pipeline in addition to the data sources used here.
