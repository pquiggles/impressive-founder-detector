import subprocess
import os
import json
from Company import *

'''
Queries Mattermark's REST endpoint for companies founded in the last day, and returns
them as "company" objects.

This class will save the results of these queries to the DATA_ROOT folder to avoid
redoing old queries.
'''
class MattermarkCompanySource:
    def __init__(self, apiKey, dataRoot):
        self.apiKey = apiKey
        self.dataRoot = dataRoot

    def getCompaniesForDate(self, date):
        if not self.__hasFileForDate(date):
            self.__downloadFileForDate(date)
            # Check to ensure download succeeded
            if not self.__hasFileForDate(date):
                raise ValueError("Downloading data for date " + date + " failed.")
        return self.__getCompaniesForDate(date)

    def __getFilename(self, date):
        return date + ".json"

    def __hasFileForDate(self, date):
        datesFound = os.listdir(self.dataRoot)
        return self.__getFilename(date) in datesFound

    def __downloadFileForDate(self, date):
        fileLocation = "%s/%s" % (self.dataRoot, self.__getFilename(date))
        cmd = 'curl -o %s --request GET --url https://api.mattermark.com/companies?added_date=%s~%s&key=%s&per_page=10000' \
        % (fileLocation, date, date, self.apiKey)
        print cmd
        args = cmd.split()
        process = subprocess.Popen(args, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = process.communicate()
        print stdout

    def __getCompaniesForDate(self, date):
        fileLocation = "%s/%s" % (self.dataRoot, self.__getFilename(date))
        text = open(fileLocation, 'r').read()
        companies = json.loads(text)['companies']
        companiesList = []
        for company in companies:
            companiesList.append(Company(company['company_name'], company['domain'], company['url']))
        return companiesList
