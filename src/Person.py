class Person:
    def __init__(self, name, profile, companies=None, schools=None, degrees=None, roles=None):
        self.profile = profile
        self.name = name
        if companies is None:
            self.companies = []
        else:
            self.companies = companies
        if schools is None:
            self.schools = []
        else:
            self.schools = schools
        if degrees is None:
            self.degrees = []
        else:
            self.degrees = degrees
        if roles is None:
            self.roles = []
        else:
            self.roles = roles
    
