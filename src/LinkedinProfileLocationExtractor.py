from bs4 import BeautifulSoup
from bs4.dammit import EncodingDetector
import urllib2
import re
import requests
import os
import simplejson
import timeout_decorator

'''
Given a company, uses the domain and name of that company to extract the Linkedin
profiles of people associated with that company.

This will check the base website itself, as well as paths from a specified list of
locations where believe we are likely to find Linkedin profiles (e.g. including
'team' will make us check 'startup.com/team')
'''
class LinkedinProfileLocationExtractor:

    def __init__(self, dataRoot, paths):
        self.dataRoot = dataRoot
        self.paths = paths

    def getProfilesForCompany(self, company):
        if not self.__hasProfilesForCompany(company):
            self.__downloadProfilesForCompany(company)
            # Check that profile file was at least created
            if not self.__hasProfilesForCompany(company):
                print "WARNING: Unable to create profile for company"
                return []
        return self.__getProfilesForCompany(company)

    def __hasProfilesForCompany(self, company):
        return os.path.isfile(self.__getCompanyProfilesLocation(company))

    # TODO: Make it so this doesn't break if two companies have the same name by including the id or something like that.
    def __getCompanyProfilesLocation(self, company):
        return (self.dataRoot + "/%s") % (company.name.replace(' ', '-'))

    def __downloadProfilesForCompany(self, company):
        profiles = []
        for path in self.paths:
            # Try both http and https, todo make this cleaner
            url = ("https://" + company.domain + "/%s") % path
            newProfiles = self.__extractProfileFromUrl(url)
            profiles.extend(newProfiles)
            url = ("http://" + company.domain + "/%s") % path
            newProfiles = self.__extractProfileFromUrl(url)
            profiles.extend(newProfiles)
        self.__writeProfiles(company, profiles)

    @timeout_decorator.timeout(5)
    def __extractProfileFromUrl(self, url):
        success = False
        try:
            # Fake the headers from a real browser since many sites will reject bots
            headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
            resp = requests.get(url, headers=headers)
            http_encoding = resp.encoding if 'charset' in resp.headers.get('content-type', '').lower() else None
            html_encoding = EncodingDetector.find_declared_encoding(resp.content, is_html=True)
            encoding = html_encoding or http_encoding
            soup = BeautifulSoup(resp.content, from_encoding=encoding)
            result = [link['href'] for link in soup.find_all('a', href=True) if "linkedin.com/in" in link['href']]
            success = True
            return result
        finally:
            if not success:
                return []

    def __writeProfiles(self, company, profiles):
        with open(self.__getCompanyProfilesLocation(company), 'w') as file_handler:
            for item in profiles:
                file_handler.write("{}\n".format(item))

    def __getProfilesForCompany(self, company):
        with open(self.__getCompanyProfilesLocation(company)) as f:
            content = f.readlines()
            content = [x.strip() for x in content]
            return content
