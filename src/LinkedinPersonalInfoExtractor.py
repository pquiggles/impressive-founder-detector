import requests
from bs4 import BeautifulSoup
import time
import os
import re
from Person import *

'''
Given a Linkedin profile's address, this class extracts info
from the profile's into person objects that include work experience and education
'''
class LinkedInPersonalInfoExtractor:
    def __init__(self, email, password, dataRoot):
        self.dataRoot = dataRoot
        self.email = email
        self.password = password
        self.client = requests.Session()
        self.headers = {'Accept-Encoding': 'identity'}
        self.__signIn()


    def __signIn(self):
        HOMEPAGE_URL = 'https://www.linkedin.com'
        LOGIN_URL = 'https://www.linkedin.com/uas/login-submit'

        html = self.client.get(HOMEPAGE_URL).content
        soup = BeautifulSoup(html)
        if soup.find(id="loginCsrfParam-login") is None:
            return
        csrf = soup.find(id="loginCsrfParam-login")['value']

        login_information = {
            'session_key': self.email,
            'session_password': self.password,
            'loginCsrfParam': csrf,
        }

        self.client.post(LOGIN_URL, data=login_information)



    def __hasInfoForProfile(self, profile):
        return os.path.isfile(self.__getInfoLocation(profile))

    def __getInfoLocation(self, profile):
        profId = 'fail'
        psplit = profile.split("/")
        if psplit[-1] == '':
            profId = psplit[-2]
        else:
            profId = psplit[-1]
        return (self.dataRoot + "/%s") % (profId)


    def getPersonalInfo(self, profileAddr):
        if not self.__hasInfoForProfile(profileAddr):
            self.__extractProfileBody(profileAddr)
            # Check that profile file was at least created
            if not self.__hasInfoForProfile(profileAddr):
                print "WARNING: Unable to create file for profile"
                return None
        body = self.__extractProfileBody(profileAddr)
        schools = self.__getSchools(body)
        companies = self.__getCompanies(body)
        degrees = self.__getDegrees(body)
        roles = self.__getRoles(body)
        firstName = self.__getFirstName(body)
        lastName = self.__getLastName(body)
        xstr = lambda s: '' if s is None else str(s)
        name = xstr(firstName) + " " + xstr(lastName)
        return Person(name, profileAddr, companies=companies, schools=schools, degrees=degrees, roles=roles)


    def __readProfile(self, profileUrl):
        with open(self.__getInfoLocation(profileUrl), 'r') as myfile:
            return myfile.read().replace('\n', '')

    def __extractProfileBody(self, profileUrl):
        response = self.client.get(profileUrl, headers=self.headers)
        time.sleep(2)
        self.__writeProfileBody(response.content, profileUrl)
        return response.content

    def __writeProfileBody(self, content, profile):
        with open(self.__getInfoLocation(profile), "w") as text_file:
            text_file.write(content)

    def __getCompanies(self, content):
        result = []
        for match in re.finditer("companyName&quot;:&quot;([^&]+)&quot;", content):
            result.append(match.group(1))
        return result

    def __getSchools(self, content):
        result = []
        for match in re.finditer("schoolName&quot;:&quot;([^&]+)&quot;", content):
            result.append(match.group(1))
        return result

    def __getDegrees(self, content):
        result = []
        for match in re.finditer("degreeName&quot;:&quot;([^&]+)&quot;", content):
            result.append(match.group(1))
        return result

    def __getRoles(self, content):
        result = []
        for match in re.finditer("title&quot;:&quot;([^&]+)&quot;", content):
            result.append(match.group(1))
        return result

    def __getFirstName(self, content):
        first = True
        for match in re.finditer("firstName&quot;:&quot;([^&]+)&quot;", content):
            if not first:
                return match.group(1)
            first = False

    def __getLastName(self, content):
        first = True
        for match in re.finditer("lastName&quot;:&quot;([^&]+)&quot;", content):
            if not first:
                return match.group(1)
            first = False
