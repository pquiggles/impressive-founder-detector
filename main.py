from src import *

# CONFIGURATION
MATTERMARK_API_KEY = "REDACTED"
MATTERMARK_DATA_ROOT = "/Users/paul.quigley/Desktop/vc/data/mattermark"
LINKEDIN_PROFILES_DATA_ROOT = "/Users/paul.quigley/Desktop/vc/data/linkedin/profile-locations"
LINKEDIN_INFO_DATA_ROOT = "/Users/paul.quigley/Desktop/vc/data/linkedin/info"
LINKEDIN_EMAIL = 'paul.isaac.quigley@gmail.com'
LINKEDIN_PASSWORD = 'REDACTED'
DATE = "2017-11-17"

# Step 1: Get companies from mattermark
mattermark = MattermarkCompanySource.MattermarkCompanySource(MATTERMARK_API_KEY, MATTERMARK_DATA_ROOT)
companies = mattermark.getCompaniesForDate(DATE)

# Step 2: Get Linkedin profiles
linkProf = LinkedinProfileLocationExtractor.LinkedinProfileLocationExtractor(LINKEDIN_PROFILES_DATA_ROOT, ['team', 'company', 'about-us', 'contact'])
companyToProfiles = {}
for company in companies:
    companyToProfiles[company] = linkProf.getProfilesForCompany(company)

# Step 3: Extract data from Linkedin profile
profExtractor = LinkedinPersonalInfoExtractor.LinkedInPersonalInfoExtractor(LINKEDIN_EMAIL, LINKEDIN_PASSWORD, LINKEDIN_INFO_DATA_ROOT)
companyToPeople = {}
for company in companies:
    companyToPeople[company] = []
    for profile in companyToProfiles[company]:
        profile = profExtractor.getPersonalInfo(profile)
        companyToPeople[company].append(profile)

# Step 4: Print out the impressive founders from each company
# Todo: Extract this logic out into a separate class
def isFounder(company, person):
    isFounder = False
    for role in person.roles:
        if "founder" in role.lower() or "ceo" in role.lower():
            isFounder = True
    worksForCompany = False
    for c in person.companies:
        if str(company.name.lower()) in str(c.lower()) or str(c.lower()) in str(company.name.lower()) :
            worksForCompany = True
    return isFounder and worksForCompany

def getMatches(personHas, desired, verbiage):
    result = []
    for p in personHas:
        pl = str(p).lower()
        for d in desired:
            dl = str(d).lower()
            if pl in dl or dl in pl:
                reason = verbiage + " " + p
                if reason not in result:
                    result.append(reason)
    return result


# EDIT impressive traits here:
roles = ["professor", "partner"]
schools = ["harvard", "stanford", "yale", "berkeley", "mit", "massachussets institute of technology", "wharton"]
degrees = ["phd", "doctorate"]
companies = ["google", "mckinsey", "facebook", "goldman", "addepar"]
def getImpressiveTraits(person):
    impressiveTraits = []
    impressiveTraits.extend(getMatches(person.roles, roles, "had title of"))
    impressiveTraits.extend(getMatches(person.degrees, degrees, "obtained degree"))
    impressiveTraits.extend(getMatches(person.companies, companies, "worked at"))
    impressiveTraits.extend(getMatches(person.schools, schools, "attended"))
    return impressiveTraits


impressiveFoundersAtCompanies = {}
for company in companyToPeople:
    impressiveFoundersAtCompany = []
    for person in companyToPeople[company]:
        if isFounder(company, person):
            impressiveReasons = getImpressiveTraits(person)
            if not len(impressiveReasons) == 0:
                impressiveFoundersAtCompany.append((person, impressiveReasons))
    impressiveFoundersAtCompanies[company] = impressiveFoundersAtCompany



for company, impressiveFounders in impressiveFoundersAtCompanies.iteritems():
    if len(impressiveFounders):
        seen = []
        for impressiveFounder in impressiveFounders:
            if not impressiveFounder[0].name in seen:
                print "%s has impressive founder %s (%s) who %s" % (company.name, impressiveFounder[0].name, impressiveFounder[0].profile, impressiveFounder[1])
                seen.append(impressiveFounder[0].name)
